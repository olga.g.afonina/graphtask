﻿using GraphTask.DataModel;
using GraphTask.Repository;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GraphTask
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}

		private void SetSplitter()
		{
			((SplitContainer)Controls[0]).SplitterDistance = grid.Width + 10;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			int[,] matrix;
			using (var context = new MatrixContext())
			{
				var repo = new Repository<Cell>(context);
				// Запишем новую матрицу 2х2
				if (repo.IsEmpty())
				{
					// Инициализировать
					matrix = new int[2, 2] { { 0, 0 }, { 0, 0 } };

					// Записать в БД
					// Columns
					for (int i = 0; i < matrix.GetLength(1); i++)
					{
						// Rows
						for (int j = 0; j < matrix.GetLength(0); j++)
						{
							repo.Create(new Cell() { ColumnIndex = i, RowIndex = j, CellValue = matrix[i, j] });
						}
					}
				}
				// Из БД
				else
				{
					var cells = repo.Get() as List<Cell>;
					var num = (int)Math.Sqrt(cells.Count);
					matrix = new int[num, num];
					foreach (var cell in cells)
					{
						matrix[cell.ColumnIndex, cell.RowIndex] = cell.CellValue;
					}
				}
			}

			grid.Build(matrix);
			canvas.Build(matrix);
			SetSplitter();
		}

		private void Grid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex != e.RowIndex)
			{
				var cell = grid[e.ColumnIndex, e.RowIndex];
				var value = cell.Value;
				cell.Value = value.ToString() == "0" ? 1 : 0;
			}
		}

		private void Grid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			canvas.Build(grid.Matrix);
		}

		private void BtnAddNode_Click(object sender, EventArgs e)
		{
			if (grid.RowCount <= 20)
			{
				grid.Add();
				canvas.Build(grid.Matrix);
			}
		}

		private void BtnDelete_Click(object sender, EventArgs e)
		{
			if (grid.RowCount > 2)
			{
				grid.Remove();
				canvas.Build(grid.Matrix);
			}
		}

		private void MainForm_Resize(object sender, EventArgs e)
		{
			if (grid.Matrix != null)
				canvas.Build(grid.Matrix);
		}

		private void Grid_SizeChanged(object sender, EventArgs e)
		{
			SetSplitter();
		}
	}
}
