﻿using GraphTask.DataModel;
using GraphTask.Repository;
using System.Drawing;

namespace System.Windows.Forms
{
	public partial class GraphGrid : DataGridView
	{
		// Ширина колонки
		private readonly int colWidth = 25;
		private int[,] _matrix;

		public int[,] Matrix { get => _matrix; }

		public GraphGrid()
		{
			AllowUserToOrderColumns = false;
			AllowUserToResizeRows = false;
			AllowUserToResizeColumns = false;
			ColumnHeadersVisible = false;
		}

		// Начальные значения в таблице
		private void Init(int[,] matrix)
		{
			ColumnCount = _matrix.GetLength(1);
			RowCount = _matrix.GetLength(0);

			foreach (var c in Columns)
			{
				((DataGridViewColumn)c).Width = colWidth;
			}

			for (int i = 0; i < ColumnCount; i++)
			{
				for (int j = 0; j < RowCount; j++)
				{
					var cell = this[i, j];
					cell.Value = matrix[i, j];
					SetCellSettings(cell, i != j);
				}
			}
		}

		private void SetCellSettings(DataGridViewCell cell, bool isActive)
		{
			if (!isActive)
			{
				cell.Style.ForeColor = Color.Gray;
				cell.Style.BackColor = Color.LightGray;
				cell.Style.SelectionForeColor = Color.Gray;
				cell.Style.SelectionBackColor = Color.LightGray;
			}
			else
			{
				cell.Style.BackColor = Color.White;
				cell.Style.ForeColor = Color.Blue;
			}

			cell.ReadOnly = true;
		}

		private void ResizeGrid()
		{
			Width = colWidth * Columns.Count + 5;
			Height = Rows[0].Height * Rows.Count + 5;
		}

		private void AddColumn()
		{
			Columns.Add("", "");
			Columns[_matrix.GetLength(0) - 1].Width = colWidth;

			for (int i = 0; i < Rows.Count; i++)
			{
				var cell = this[Columns.Count - 1, i];
				cell.Value = 0;
				SetCellSettings(cell, true);
			}
		}

		private void RemoveColumn()
		{

			var columnIndex = Columns.Count - 1;

			using (var context = new MatrixContext())
			{
				var repo = new Repository<Cell>(context);
				for (int i = 0; i < Rows.Count; i++)
				{
					var cell = repo.Get(x => x.RowIndex == i && x.ColumnIndex == columnIndex);
					
					if (cell != null)
						repo.Remove(cell);
				}
			}

			Columns.RemoveAt(columnIndex);
		}

		private void AddRow()
		{
			Rows.Add();

			for (int i = 0; i < Columns.Count; i++)
			{
				var cell = this[i, Rows.Count - 1];
				cell.Value = 0;
				SetCellSettings(cell, true);
			}
		}

		private void RemoveRow()
		{
			var rowIndex = Rows.Count - 1;

			using (var context = new MatrixContext())
			{
				var repo = new Repository<Cell>(context);
				for (int i = 0; i < Columns.Count; i++)
				{
					var cell = repo.Get(x => x.RowIndex == rowIndex && x.ColumnIndex == i);
					if (cell != null)
						repo.Remove(cell);
				}
			}

			Rows.RemoveAt(rowIndex);
		}

		public void Build(int[,] matrix)
		{
			_matrix = matrix;
			Init(_matrix);
			ResizeGrid();
		}

		public void Add()
		{
			var num = _matrix.GetLength(0);
			var newMatrix = new int[num + 1, num + 1];
			for (int i = 0; i < num + 1; i++)
			{
				for (int j = 0; j < num + 1; j++)
				{
					if (i == num || j == num)
						newMatrix[i, j] = 0;
					else
						newMatrix[i, j] = _matrix[i, j];
				}
			}

			_matrix = newMatrix;
			AddColumn();
			AddRow();
			SetCellSettings(this[Columns.Count - 1, Rows.Count - 1], false);
			ResizeGrid();
		}

		public void Remove()
		{
			var num = _matrix.GetLength(0);
			var newMatrix = new int[num - 1, num - 1];
			for (int i = 0; i < num - 1; i++)
			{
				for (int j = 0; j < num - 1; j++)
				{
					newMatrix[i, j] = _matrix[i, j];
				}
			}

			_matrix = newMatrix;
			RemoveColumn();
			RemoveRow();
			ResizeGrid();
		}

		protected override void OnPaint(PaintEventArgs pe)
		{
			base.OnPaint(pe);
		}

		protected override void OnCellValueChanged(DataGridViewCellEventArgs e)
		{
			try
			{
				var cell = this[e.ColumnIndex, e.RowIndex];
				var value = int.Parse(cell.Value == null ? "0" : cell.Value.ToString());
				_matrix[e.ColumnIndex, e.RowIndex] = value;

				// Изменим в БД
				using (var db = new MatrixContext())
				{
					var repo = new Repository<Cell>(db);
					var current = repo.Get(x => x.RowIndex == e.RowIndex && x.ColumnIndex == e.ColumnIndex);
					if (current != null)
					{
						current.CellValue = value;
						repo.Update(current);
					}
					else
					{
						// Начальное значение инициируем нулем
						repo.Create(new Cell() { RowIndex = e.RowIndex, ColumnIndex = e.ColumnIndex, CellValue = 0 });
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
			base.OnCellValueChanged(e);
		}
	}
}
