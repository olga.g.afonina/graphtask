﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace GraphTask
{
	public class Node
	{
		// Список вершин, с которыми соединена данная вершина
		public readonly List<Node> IncidentNodes = new List<Node>();
		public string Label;
		public float Radius = 15f;
		public PointF Location;

		public void Drag(PointF offset)
		{
			Location = Location.Add(offset);
		}

		public void EndDrag() { }

		public bool Hit(PointF point)
		{
			var p = point.Sub(Location);
			return Math.Pow(p.X, 2) + Math.Pow(p.Y, 2) <= Radius * Radius;
		}

		public void Paint(Graphics gr)
		{
			foreach (var item in IncidentNodes)
				gr.DrawLink(Radius, Location, item.Location);

			var state = gr.Save();
			gr.TranslateTransform(Location.X, Location.Y);
			gr.DrawCircle(Radius, Pens.Black);
			if (!string.IsNullOrEmpty(Label))
			{
				gr.DrawText(Label, SystemFonts.CaptionFont, SystemBrushes.ControlDarkDark, Radius);
			}
			gr.Restore(state);
		}
	}
}
