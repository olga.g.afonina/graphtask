﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace System.Windows.Forms
{
	public static class DrawingExtentions
	{
		private static readonly Pen LinkPen = new Pen(Color.Red) { CustomEndCap = new AdjustableArrowCap(5, 10, false) };

		public static PointF Sub(this PointF pt1, PointF pt2)
		{
			return new PointF(pt1.X - pt2.X, pt1.Y - pt2.Y);
		}

		public static PointF Add(this PointF pt1, PointF pt2)
		{
			return new PointF(pt1.X + pt2.X, pt1.Y + pt2.Y);
		}

		public static PointF Mid(this PointF pt1, PointF pt2)
		{
			return new PointF((pt2.X + pt1.X) / 2, (pt2.Y + pt1.Y) / 2);
		}

		public static void DrawLink(this Graphics g, float radius, PointF from, PointF to)
		{
			var pt1 = new Vector(to.X - from.X, to.Y - from.Y);
			pt1.Normalize();
			pt1 *= radius;
			var pt2 = new Vector(from.X - to.X, from.Y - to.Y);
			pt2.Normalize();
			pt2 *= radius;
			pt1 = pt1.Add(from);
			pt2 = pt2.Add(to);
			g.DrawLine(LinkPen, pt1.ToPointF(), pt2.ToPointF());
		}

		private static Vector Add(this Vector vector, PointF point)
		{
			return new Vector(vector.X + point.X, vector.Y + point.Y);
		}

		private static PointF ToPointF(this Vector vector)
		{
			return new PointF((float)vector.X, (float)vector.Y);
		}

		public static void DrawCircle(this Graphics g, float radius, Pen pen)
		{
			g.DrawEllipse(pen, -radius, -radius, 2 * radius, 2 * radius);
			g.FillEllipse(new SolidBrush(Color.White), -radius, -radius, 2 * radius, 2 * radius);
		}

		public static void DrawText(this Graphics g, string text, Font font, Brush brush, float radius)
		{
			var rect = new RectangleF(-radius, -radius, 2 * radius, 2 * radius);
			rect.Inflate(-3, -3);
			g.DrawString(text, font, brush, rect, new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center });
		}
	}
}
