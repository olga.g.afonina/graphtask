﻿using System;
using System.Data.Entity;
using System.Linq;

namespace GraphTask.DataModel
{
	public class MatrixContext : DbContext
	{
		public MatrixContext()
			: base("name=MatrixContext")
		{
		}

		public DbSet<Cell> Cells { get; set; }
	}
}