﻿using System.ComponentModel.DataAnnotations;

namespace GraphTask.DataModel
{
	public class Cell
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public int ColumnIndex { get; set; }

		[Required]
		public int RowIndex { get; set; }

		public int CellValue { get; set; }
	}
}
