﻿using GraphTask;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;

namespace System.Windows.Forms
{
	public partial class Canvas : Panel
	{
		private IList<Node> _nodes;
		private PointF _offset;
		private PointF Center { get => new PointF(ClientRectangle.Width / 2f, ClientRectangle.Height / 2); }

		public Canvas()
		{
			SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.Selectable, true);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			if (_nodes == null) return;
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			e.Graphics.TranslateTransform(Center.X, Center.Y);
			e.Graphics.TranslateTransform(_offset.X, _offset.Y);

			foreach (var item in _nodes)
				item.Paint(e.Graphics);
		}

		public void Build(int[,] matrix)
		{
			CreateNodes(matrix);
			Invalidate();
		}

		private void CreateNodes(int[,] matrix)
		{
			_nodes = Enumerable.Range(0, matrix.GetLength(0)).Select(i => new Node { Label = (i + 1).ToString() }).ToList();

			for (int i = 0; i < matrix.GetLength(0); i++)
				for (int j = 0; j < matrix.GetLength(1); j++)
				{
					if (matrix[i, j] != 0)
						_nodes[i].IncidentNodes.Add(_nodes[j]);
				}
			ArrangeNodes();
		}

		private void ArrangeNodes()
		{
			int i = 0;
			var r = Math.Min(ClientRectangle.Width, ClientRectangle.Height) * 0.8 / 2f;
			int count = _nodes.Count;
			foreach (var node in _nodes.Cast<Node>())
			{
				var x = r * Math.Cos(2 * Math.PI * i / count);
				var y = r * Math.Sin(2 * Math.PI * i / count);
				node.Location = new PointF((float)x, (float)y);
				i++;
			}
		}
	}
}
